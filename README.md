# UU INFOB2DA - Data Analytics

This repository holds the UU INFOB2DA - Data Analytics website.

- The 2022 website is hosted in the repository "2022" and automatically built by jekyll on Gitlab
- The 2021 website is hosted in the repository "2021" and automatically built by jekyll on Gitlab
- The 2020 website is hosted in the repository "2020" and automatically built by jekyll on Gitlab
- The index.html file automatically redirects to the latest year.

## Last Changes

- June 2021: Changed to 2021 website
- Sept 2022: Changed to 2022 website

# Editor

Michael Behrisch, [https://michael.behrisch.info](https://michael.behrisch.info)


